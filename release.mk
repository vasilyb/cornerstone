#This file is supposed to:
#o automatic creating new releases of the port,
#o automatic creating tarballs of new versions.
#
#Requirements
#o sudo must be installed
#o you must be permitted to run `make` via `sudo`
#
#Use the next commands to publish the tarball and update port:
#  make -f release.mk tarball port clean
# or
#  make -f release.mk all
#

###################################################################
#Variables
PORTNAME=		cornerstone
PORTVERSION!=	expr "`cat Makefile | grep PORTVERSION`" : 'PORTVERSION=[^0-9.]*\(.*\)'
PORTID=			${PORTNAME}-${PORTVERSION}
TMPDIR=			/tmp/${PORTNAME}/
TARBALL_TMPDIR=	${TMPDIR}tarball/
TARBALL_SRCDIR=	${TARBALL_TMPDIR}${PORTID}/
TARBALL_NAME=	${PORTID}.tar.gz
TARBALL_PATH=	${TARBALL_TMPDIR}${TARBALL_NAME}
TARBALL_MAN=	${TARBALL_SRCDIR}/man/${PORTNAME}.1
PORT_TMPDIR=	${TMPDIR}port/
PORT_NAME=		${PORTID}.port.tar.gz
PORT_PATH=		${PORT_TMPDIR}${PORT_NAME}
WIKI_CLONEDIR=	../
WIKI_CLONENAME=	${PORTNAME}.wiki/
WIKI_CLONEPATH=	${WIKI_CLONEDIR}${WIKI_CLONENAME}
DOWNLOADSDIR=	downloads/
DISTDIR=		/usr/ports/distfiles/

#These files must be in the FreeBSD port
PORT_FILES=		'Makefile'\
				'distinfo'\
				'pkg-descr'\
				'pkg-plist'

COMPOSER_FILES=	'composer-setup.php'\
				'composer.phar'\
				'vendor'

#These files mustn't be in the tarball
TARBALL_IGNORE=	'.git'\
				'.gitignore'\
				'release.mk'\
				'debug.php'\
				${PORT_FILES}\
				${COMPOSER_FILES}


#Messages and warnings
RSAPASSW=		echo '... YOU MAY BE PROMPTED FOR THE PASSWORD FROM THE RSA KEY (FOR git)'
SUDOPASSW=		echo '... YOU MAY BE PROMPTED FOR THE PASSWORD FROM YOUR USER ACCOUNT (FOR sudo)'

#SEd script for manual page updating
MAN_TH_SEARCH=	.TH CORNERSTONE 1 "March 2017" "Cornerstone 0.1"
MAN_TH_DATE!=	export LC_ALL=en_US.UTF-8 && date +'%B %Y'
MAN_TH_ACTUAL=	.TH CORNERSTONE 1 "${MAN_TH_DATE}" "Cornerstone ${PORTVERSION}"
SED_UPDMAN=		's/${MAN_TH_SEARCH}/${MAN_TH_ACTUAL}/'

###################################################################
#Targets for internal usage

#Create a temporary sub-directory
prepare:
	@echo -n 'Creating a temporary directory... ' &&\
	mkdir -p ${TMPDIR} &&\
	echo 'done'

#Clone the project wiki repository
pull-wiki:
	@if [ -d ${WIKI_CLONEPATH} ]\
	;then\
	 echo 'Pulling latest changes to the wiki project clone... ' &&\
	 cd ${WIKI_CLONEPATH} &&\
	 ${RSAPASSW} &&\
	 git pull -q &&\
	 cd - &&\
	 echo '... done'\
	;else\
	 echo 'Clonning the wiki project repository... ' &&\
	 git clone -q\
	  git@gitlab.com:vasilyb/${PORTNAME}.wiki\
	  ${WIKI_CLONEPATH} &&\
	 echo '... done'\
	;fi

###################################################################
#Tarball-related internal targets

#Create the tarball
create-tarball: prepare
	@echo -n 'Creating a working directory for tarball creation... ' &&\
	mkdir -p ${TARBALL_TMPDIR} &&\
	echo 'done'
	@echo -n 'Creating a sub-directory inside the tarball directory... ' &&\
	mkdir -p ${TARBALL_SRCDIR} &&\
	echo 'done'
	@echo -n 'Copying files to include in a tarball... ' &&\
	for file in `ls`\
	;do\
	 if [ `echo "${TARBALL_IGNORE}" | grep -c "'$${file}'"` -eq 0 ]\
	 ;then\
	  keys= &&\
	  if [ -d "$${file}" ] ;then keys="$${keys}-R" ;fi &&\
	  cp $${keys}$${keys:+ }"$${file}" ${TARBALL_SRCDIR}\
	 ;fi\
	;done &&\
	echo 'done'
	@echo -n 'Updating man page... ' &&\
	sed -e ${SED_UPDMAN} <${TARBALL_MAN} >${TARBALL_MAN}.out &&\
	rm ${TARBALL_MAN} &&\
	mv ${TARBALL_MAN}.out ${TARBALL_MAN} &&\
	echo 'done'
	@echo -n 'Creating a tarball archive... ' &&\
	cd ${TARBALL_TMPDIR} &&\
	tar czf ${TARBALL_NAME} ${PORTID} &&\
	cd - &&\
	echo 'done'

#Push the created tarball to the remote repository of project wiki
push-tarball: pull-wiki
	@echo -n 'Copying a newer version of the tarball... ' &&\
	cp ${TARBALL_PATH} ${WIKI_CLONEPATH}${DOWNLOADSDIR} &&\
	echo 'done' &&\
	echo -n 'Staging the newer version of the tarball for commit... ' &&\
	cd ${WIKI_CLONEPATH} &&\
	git add ${DOWNLOADSDIR}${TARBALL_NAME} > /dev/null &&\
	echo 'done' &&\
	echo -n 'Commiting the new version of the tarball... ' &&\
	git commit -q -m "Tarball has been updated using release.mk at `date +%y-%m-%d`" &&\
	echo 'done' &&\
	echo 'Pushing the changes to the remote repository... ' &&\
	${RSAPASSW} &&\
	git push -q &&\
	cd - &&\
	echo 'done'

#Clean up the tarball creation
clean-tarball:
	@echo -n 'Removing the files included into the tarball... ' &&\
	rm -rf ${TARBALL_SRCDIR}* &&\
	echo 'done'
	@echo -n 'Removing the sub-directory from the tarball directory... ' &&\
	if [ -d ${TARBALL_SRCDIR} ] ;then rmdir ${TARBALL_SRCDIR} ;fi &&\
	echo 'done'
	@echo -n 'Removing the tarball archive... ' &&\
	if [ -f ${TARBALL_PATH} ] ;then rm ${TARBALL_PATH} ;fi &&\
	echo 'done'
	@echo -n 'Removing the working directory after tarball creation... ' &&\
	if [ -d "${TARBALL_TMPDIR}" ] ;then rmdir ${TARBALL_TMPDIR} ;fi &&\
	echo 'done'

###################################################################
#Port-related internal targets

#The next target will check if the sudo installed and create a distinfo file
make-checksum:
	@echo -n 'Checking if the sudo is installed... ' &&\
	if ! sudo -V > /dev/null 2>&1 ;then\
	 echo 'not found! The sudo is required in order to create the port. Please, install it first!' &&\
	 false\
	;fi &&\
	echo 'done'
	@echo -n 'Removing old distinfo... ' &&\
	rm -f distinfo &&\
	echo 'done'
	@echo 'Removing old distfile... ' &&\
	${SUDOPASSW} &&\
	sudo rm -f ${DISTDIR}${TARBALL_NAME} &&\
	echo 'done'
	@echo 'Creating checksum... ' &&\
	${SUDOPASSW} &&\
	sudo make makesum &&\
	echo '... done'
	@echo 'Removing the distfile after the distinfo creation... ' &&\
	${SUDOPASSW} &&\
	sudo rm -f ${DISTDIR}${TARBALL_NAME} &&\
	echo '... done'

#This target creates the port archive
create-port: prepare make-checksum
	@echo -n 'Creating a directory for a port creation... ' &&\
	mkdir -p ${PORT_TMPDIR} &&\
	echo 'done'
	@echo -n 'Copying files for including to the port... ' &&\
	for file in ${PORT_FILES} \
	;do cp "$${file}" ${PORT_TMPDIR}\
	;done &&\
	echo 'done'
	@echo -n 'Creating the port archive... ' &&\
	cd ${PORT_TMPDIR} &&\
	tar czf ${PORT_NAME} * &&\
	cd - &&\
	echo 'done'

#The next target is to publish the port archive to the wiki repo
push-port: pull-wiki
	@echo -n 'Copying a newer version of the port... ' &&\
	cp ${PORT_PATH} ${WIKI_CLONEPATH}${DOWNLOADSDIR} &&\
	echo 'done' &&\
	echo -n 'Staging the newer version of the port for commit... ' &&\
	cd ${WIKI_CLONEPATH}${DOWNLOADSDIR} &&\
	git add ${PORT_NAME} > /dev/null &&\
	echo 'done' &&\
	echo -n 'Commiting the new version of the port... ' &&\
	git commit -q -m "Port has been updated using release.mk at `date +%y-%m-%d`" &&\
	echo 'done' &&\
	echo 'Pushing the changes to the remote repository... ' &&\
	${RSAPASSW} &&\
	git push -q &&\
	cd - &&\
	echo '... done'

#This target cleans up the system after the port is created
clean-port:
	@echo -n 'Cleaning the port creation directory... ' &&\
	rm -f ${PORT_TMPDIR}* &&\
	echo 'done'
	@echo -n 'Removing the directory after the port creation... ' &&\
	if [ -d "${PORT_TMPDIR}" ] ;then rmdir ${PORT_TMPDIR} ;fi &&\
	echo 'done'

###################################################################
#This target creates and publishes the tarball
tarball: create-tarball push-tarball

###################################################################
#This target is to create and publish the port
port: create-port push-port

##################################################################
#The next target creates a full release and cleans up
all: tarball port clean

###################################################################
#Cleans up all the working files and directories
clean: clean-tarball clean-port
	@echo -n 'Removing the temporary directory... ' &&\
	rmdir ${TMPDIR} &&\
	echo 'done'

