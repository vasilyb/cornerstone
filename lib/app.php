<?php namespace cornerstone;

class App {
	static function run($argv) {
		$cmnd = Args::parse($argv);
		$cmnd->exec();
	} // function run
} // class App

?>
