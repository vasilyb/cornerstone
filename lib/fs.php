<?php namespace cornerstone;

class Fs { // file system helper
	static function makePath($path) {
		if (! file_exists($path)):
			if (! mkdir($path, 0700, true)):
				global $sys;
				$sys->terminate("Failed to create config at '$path'", ERR_STORAGE);
			endif; // ! mkdir
		endif; // ! file_exists
	} // makePath()
	static function dirName($dir) {
		// makes the path looking as a directory name by adding
		// a trailing slash to it
		if (! isset($dir) || $dir == ''):
			$dir = '.';
		endif; // ! $dir
		if ($dir[strlen($dir)-1] != FS_PATH_SEPARATOR):
			$dir = "$dir".FS_PATH_SEPARATOR;
		endif; // ! $dir ends with /
		return $dir;
	} // dirName()
	static function fileName($dir, $name, $ext = '') {
		// validate file name
		if (empty($name)):
			global $sys;
			$sys->terminate('File must have a name', ERR_STORAGE);
		endif; // ! $name
		// process target directory
		$dir = Fs::dirName($dir);
		// process file extension
		if (! empty($ext) && $ext[0] != FS_EXTENSION_SEPARATOR):
			$ext = FS_EXTENSION_SEPARATOR."$ext";
		endif; // ! ext starts with .
		return "$dir$name$ext";
	} // fileName()
	static function fileEcho($path, $content) {
		global $sys;
		$dir = dirname($path);
		Fs::makePath($dir);
		$item_file_res = fopen($path, 'w');
		if (! $item_file_res):
			$sys->terminate("Error on opening '$path' for writing", ERR_STORAGE);
		endif; // ! $item_file_res
		fwrite($item_file_res, $content);
		if (! fclose($item_file_res)):
			$sys->terminate("Unable to properly close '$path'", ERR_STORAGE);
		endif; // ! fclose
	} // fileEcho()
} // class Fs

?>
