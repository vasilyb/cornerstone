<?php namespace cornerstone;

class Storage {
	private $conf;
	private $auto_inc_file_name;
	private $todos = 'todo';
	private $events = 'event';
	private $journal = 'journal';
	function __construct($dir) {
		$this->conf = $dir;
		$this->auto_inc_file_name = Fs::dirName($this->conf).'.inc';
		$this->todos = "$dir/$this->todos";
		$this->events = "$dir/$this->events";
		$this->journal = "$dir/$this->journal";
	} // function __construct
	function of($type, $state) {
		global $sys;
		if (! in_array($type,
			array(ITEM_TYPE_TODO, ITEM_TYPE_EVENT, ITEM_TYPE_JOURNAL))
		):
			$sys->terminate("The '$type' is a wrong item type", ERR_STORAGE);
		endif; // ! $type
		if (! item\State::test($state)):
			$sys->terminate("The '$state' is a wrong item state", ERR_STORAGE);
		endif; // wrong $state
		return $this->$type."/$state";
	} // function of
	function getHead() {
		global $sys;
		$inc_file_name = $this->auto_inc_file_name;
		if (! file_exists($inc_file_name)):
			return 0;
		else: // file_exists
			$inc_file_res = fopen($inc_file_name, 'r');
			if (! $inc_file_res):
				$sys->terminate("Failed opening '$inc_file_name' for read", ERR_STORAGE);
			endif; // ! $inc_file_res
			$maxid = fread($inc_file_res, filesize($inc_file_name));
			if (! fclose($inc_file_res)):
				$sys->terminate("Unable to properly close '$inc_file_name'", ERR_STORAGE);
			endif; // ! fclose
			return $maxid;
		endif; // ! file_exists
	} // getHead()
	function setHead($number) {
		global $sys;
		$inc_file_name = $this->auto_inc_file_name;
		$inc_file_res = fopen($inc_file_name, 'w');
		if (! $inc_file_res):
			$sys->terminate("Failed opening '$inc_file_name' for write", ERR_STORAGE);
		endif; // ! $inc_file_res
		if (! fwrite($inc_file_res, $number)):
			$sys->terminate("Writing the '$inc_file_name' failed", ERR_STORAGE);
		endif; // ! fwrite
		if (! fclose($inc_file_res)):
			$sys->terminate("Unable to properly close '$inc_file_name'", ERR_STORAGE);
		endif; // ! fclose
	} // setHead()
} // class Storage

?>
