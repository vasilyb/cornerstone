<?php namespace cornerstone;

// this file contains named constants

// to use in the file system helper class
define('FS_PATH_SEPARATOR', '/');
define('FS_EXTENSION_SEPARATOR', '.');

// types of items and according directories names
define('ITEM_TYPE_TODO', 'todos');
define('ITEM_TYPE_EVENT', 'events');
define('ITEM_TYPE_JOURNAL', 'journal');

// states of items and according directories names
define('ITEM_STATE_OPEN', 'open');
define('ITEM_STATE_DONE', 'done');
define('ITEM_STATE_TRASH', 'trash');

// keys of item properties
define('ITEM_PROP_ID', 'id');
define('ITEM_PROP_TITLE', 'title');
define('ITEM_PROP_STATE', 'state');

// files extensions
define('EXT_ITEM', 'xml');
define('EXT_SCRIPT', 'php');

// for work with classes and namespaces
// use only lowercase for ns names
define('NS_DELIMITER', "\\");

// to use in various formatting functions
define('FMT_ESC_START', "\\");
define('FMT_MATCH_FULL', 0);
define('FMT_MATCH_STRING', 0);
define('FMT_MATCH_OFFSET', 1);

// to use in command line parsing algorithms
define('MAIN_ARG_MIN_IDX', 2); // 0=command, 1=subcommand
define('CLI_PARAMVAL_DELIMITER', ',');
define('CLI_KEYNAME_PATTERN', '/(?<=--)[a-z-]+(?==?)/');

// to use in commands factory code
define('CMND_DEFAULT', 'list');
define('CMND_NAME', 1);

// exit codes
define('ERR_DEBUG', 2);
define('ERR_XML', 4);
define('ERR_CLI', 6);
define('ERR_STORAGE', 8);
define('ERR_ITEM', 10);
define('ERR_SYS', 12);
define('ERR_CMND', 14);
define('ERR_UNKNOWN', 254);

?>
