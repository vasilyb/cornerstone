<?php namespace cornerstone;
use cornerstone\item\Formatter as Formatter;

class Item {
	private $properties;
	static function fileRead($path) {
		$reader = new item\Reader;
		return $reader->fileRead($path);
	} // fileRead()
	function fileWrite($dir) {
		$writer = new item\Writer($this);
		return $writer->fileWrite($dir);
	} // fileWrite()
	function __construct($title = NULL, $id = NULL) {
		// init properties
		$prop_keys = array(ITEM_PROP_ID, ITEM_PROP_TITLE, ITEM_PROP_STATE);
		foreach ($prop_keys as $key):
			$this->properties[$key] = NULL;
		endforeach;
		// set properties values
		$this->title = $title;
		$this->id = $id;
	} // function __construct
	function __set($key, $value) {
		if (! array_key_exists($key, $this->properties)):
			global $sys;
			$sys->terminate("The '$key' does not exist in the array", ERR_ITEM);
		endif; // ! array_key_exists
		$this->properties[$key] = $value;
	} // function __set
	function __get($key) {
		// computed meta properties
		// data properties
		if (! array_key_exists($key, $this->properties)):
			global $sys;
			$sys->terminate("The '$key' does not exist in the array", ERR_ITEM);
		endif; // ! array_key_exists
		return $this->properties[$key];
	} // function __get
	function format($format) {
		$formatter = new Formatter($this->properties);
		return $formatter->format($format);
	} // function format
} // class Item

?>
