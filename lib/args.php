<?php namespace cornerstone;

class Args {
	private static function command($args) {
		// this function returns the command entered by user (or default one)
		$cmnd_name =
			(! isset($args) or ! is_array($args) or count($args) < 2)
			? CMND_DEFAULT
			: $args[CMND_NAME];
		if (strlen($cmnd_name) == 0):
			$cmnd_name = CMND_DEFAULT;
		endif; // zero length command name
		return $cmnd_name;
	} // command()
	private static function mainArg($args) {
		if (! is_array($args)):
			return null;
		endif; // $args is not array
		$main_arg_idx = sizeof($args) - 1;
		if ($main_arg_idx < MAIN_ARG_MIN_IDX):
			return null;
		endif; // no main argument
		$arg = $args[$main_arg_idx];
		if (1 === preg_match(CLI_KEYNAME_PATTERN, $arg)):
			return null;
		endif; // $arg is the key
		return $arg;
	} // mainArg()
	private static function options($args) {
		$options = array();
		if (! is_array($args)):
			return $options;
		endif; // ! is_array
		$args = array_slice($args, 2);
		$i = 0;
		while ($i < sizeof($args)):
			if (1 === preg_match(CLI_KEYNAME_PATTERN, $args[$i], $matches)):
				$key = $matches[0];
				$keyval_pattern = "/(?<=--$key=).*/";
				$value = 1 === preg_match(
					$keyval_pattern, $args[$i], $matches)?
					$matches[0]:
					NULL;
				$options[$key] = $value;
			endif; // preg_match
			$i = $i + 1;
		endwhile; // sizeof($args)
		return $options;
	} // functions options
	static function parse($args) {
		// get command details:
		$cmnd_name = Args::command($args);
		$options = Args::options($args);
		$argument = Args::mainArg($args);
		// transform command name to according class name:
		$cmnd2class = new Cmnd2class;
		$class_name = $cmnd2class->transform($cmnd_name);
		// create command instance (or the Wrong cmnd if class wasn't loaded)
		if (class_exists($class_name)):
			return new $class_name($options, $argument);
		else: // class does not exist
			if (! class_exists('cornerstone\cmnd\Wrong')):
				global $sys;
				$sys->terminate('Unable to find the command to say you '.
						"that the `$class_name` class ".
						'was not found anywhere it may be.',
					ERR_CLI);
			endif; // the `wrong` command does not exist
			return new cmnd\Wrong(array(), $cmnd_name);
			// $1!=NULL to avoid crashes
		endif; // class exists
	} // function parse
} // class Args

?>
