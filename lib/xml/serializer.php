<?php declare(strict_types=1);

namespace cornerstone\xml;
use \Xmtk\Writer;

class Serializer extends Writer {
	protected function error(string $message) {
		global $sys;
		$sys->terminate($message, ERR_XML);
	} // error()
} // Serializer
?>