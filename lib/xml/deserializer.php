<?php declare(strict_types=1);

namespace cornerstone\xml;
use \Xmtk\Parser;

class Deserializer extends Parser {
	protected function error(string $message) {
		global $sys;
		$sys->terminate($message, ERR_XML);
	} // error()
} // Deserializer
?>
