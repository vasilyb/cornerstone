<?php namespace cornerstone;

// library classes auto loader function
function autoload($ftype, $class) {
	if (empty($class)):
		return; // the class full name to be auto loaded is empty
	endif; // class full name is empty
	global $sys;
	$callable = array($sys, $ftype);
	// split namespaces and class names by one
	$class_path = explode(NS_DELIMITER, $class);
	// the root namespace must be the 'cornerstone'
	unset($class_path[0]);
	$class_path = array_values($class_path);
	// make filename lowercase and add extension
	$classname_idx = sizeof($class_path)-1;
	$filename_without_extension = 
		CaseTransformer::getInstance()->studlyCaps2underScore(
			$class_path[$classname_idx]);
	$class_path[$classname_idx] = 
		$filename_without_extension.FS_EXTENSION_SEPARATOR.EXT_SCRIPT;
	// glue file path pieces we got from full class name back
	$filepath_namespaced = call_user_func($callable,
		implode(FS_PATH_SEPARATOR, $class_path));
	if (file_exists($filepath_namespaced)):
		include_once $filepath_namespaced; // found by namespace without root one
	elseif (file_exists($filepath_shortened
		= call_user_func($callable, $class_path[$classname_idx]))):
		include_once $filepath_shortened; // found by only class name
	elseif (count($class_path) > 1 && file_exists($filepath_average
		= call_user_func($callable, 
			implode(FS_PATH_SEPARATOR, array_slice($class_path, 1))))):
		include_once $filepath_average; // found by namespace without two first parts
	else /*file containing the requested class did not found*/:
		return; // $filepath_namespaced and $filepath_shortened are both empty
	endif; // including file with the requested $class
} // cornerstone\autoload()

function autoload_library($class) {
	autoload('lib', $class);
} // cornerstone\autoload_library()

function autoload_command($class) {
	autoload('cmnd', $class);
} // cornerstone\autoload_command()

// composer autoload
include_once __DIR__.FS_PATH_SEPARATOR.$sys->vendor_dir.'autoload.php';

// custom auto loaders registration
spl_autoload_register('cornerstone\\autoload_library')
	or $sys->terminate('Unable to register function for autoload libraries.', ERR_SYS);
spl_autoload_register('cornerstone\\autoload_command')
	or $sys->terminate('Unable to register function for autoload commands.', ERR_SYS);

?>
