<?php namespace cornerstone\cmnd\base;

abstract class Cmnd {
	private $confs = '.config';
	private $conf = 'cornerstone';
	protected $storage;
	protected $options;
	function __construct($options = array(), $main_arg = NULL) {
		$home = getenv('HOME');
		$this->confs = "$home/$this->confs";
		$this->conf = "$this->confs/$this->conf";
		$this->storage = new \cornerstone\Storage($this->conf);
		$this->options = $options;
	} // function __construct
	abstract function exec();
} // class Cmnd

?>
