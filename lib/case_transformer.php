<?php namespace cornerstone;

class CaseTransformer {
	static private $singletone;
	private function __construct() {
	} // __construct()
	private function __clone() {
	} // __clone
	public function __wakeup() {
	} // __wakeup();
	static function getInstance() {
		if (! isset(CaseTransformer::$singletone)):
			CaseTransformer::$singletone = new CaseTransformer;
		endif; // initialize singletone transformer if not done yet
		return CaseTransformer::$singletone;
	} // getInstance()

	function studlyCaps2underScore($in) {
		// ClassName ~> class_name
		if (! is_string($in)):
			global $sys;
			$sys->terminate(__METHOD__.': $in must be a string.', ERR_SYS);
		endif; // input argument must be a string
		$in = str_split($in); // string to array of letters
		$out = '';
		foreach ($in as $chr):
			if ((1 === preg_match('/[A-Za-z]/', $chr))
				&& (strtoupper($chr) === $chr)):
				// current character is upper case
				if (strlen($out)):
					$out .= '_';
				endif; // result in not an empty string already
				$out .= strtolower($chr);
			else: // current character is lower case
				$out .= $chr;
			endif;
		endforeach; // $chr in $in
		return $out;
	} // studlyCaps2underScore()
} // class CaseTransformer

?>
