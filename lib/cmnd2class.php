<?php namespace cornerstone;

// to transform commands to according class names
class Cmnd2class {
	// commands namespace
	private $ns = __NAMESPACE__.NS_DELIMITER.'cmnd'.NS_DELIMITER;
	// special mapping (private, plugins should not use this hack)
	private $exclusions = array(
		'list' => 'Lst'
	); // exceptional mappings for cases when class name != command name
	private function getClass($cmnd) {
		if (array_key_exists($cmnd, $this->exclusions)):
			return $this->exclusions[$cmnd];
		endif; // command != class
		$excluded_classes = array();
		foreach ($this->exclusions as $exclusion):
			$excluded_classes []= strtolower($exclusion);
		endforeach; // lowering exclusion classes
		if (! in_array($cmnd, $excluded_classes)):
			return strtoupper(substr($cmnd,0,1)).substr($cmnd,1,strlen($cmnd)-1);
		endif; // not excluded, not exclusion
		return NULL;
	} // getClass()
	function transform($cmnd) {
		$class = $this->getClass($cmnd);
		return isset($class)? $this->ns.$class: NULL;
	} // transform()
} // class Cmnd2class

?>
