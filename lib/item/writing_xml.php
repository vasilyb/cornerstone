<?php namespace cornerstone\item;
use cornerstone\Item as Item;
use cornerstone\Fs as Fs;
use cornerstone\xml\Serializer as Serializer;
use Xmtk\CData;

class WritingXml implements WritingInterface
{
	private $item;
	function __construct($item) {
		$this->item = $item;
	} // __construct()
	private function serializeFileXml() {
		$cdata = (new CData)->encode($this->item->title);
		$xml = array(
			'item' => array(
				ITEM_PROP_TITLE => $cdata
			) // <item>
		);
		$ser = new Serializer;
		return $ser->xmlWriteFromArray($xml);
	} // serializeFileXml()
	function fileWrite($dir) {
		// if $this->id === 10, empty($this->id) !== empty(10)
		$id = $this->item->id; $title = $this->item->title;
		if (empty($dir) || empty($id) || empty($title)):
			return FALSE;
		endif; // check props required for file writing
		Fs::fileEcho(
			Fs::fileName($dir, $id, EXT_ITEM),
			$this->serializeFileXml()); // fileEcho()
		return TRUE;
	} // fileWrite()
} // WritingXml

?>
