<?php namespace cornerstone\item;

class State {
	public $state;
	public $name;
	static function test($state) {
		return in_array($state, State::all());
	} // function to test state
	static function all() {
		return array(ITEM_STATE_OPEN, ITEM_STATE_DONE, ITEM_STATE_TRASH);
	} // function all()
	function __construct($state, $name) {
		if (! State::test($state)):
			global $sys;
			$sys->terminate("The '$state' is a wrong item state", ERR_ITEM);
		endif; // wrong state
		$this->state = $state;
		$this->name = $name;
	} // function __construct
} // class State

?>
