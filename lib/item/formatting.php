<?php namespace cornerstone\item;
use cornerstone\Item as Item;
use cornerstone\Fs as Fs;

// default formatting for items (the strategy pattern used)
class Formatting implements FormattingInterface
{
	private $placeholders;
	private $properties;
	private $placeholders_props;
	function __construct($props) {
		// placeholder-property pairs
		$this->placeholders_props = array
		(
			'i' => ITEM_PROP_ID,
			't' => ITEM_PROP_TITLE,
			's' => ITEM_PROP_STATE
		);
		$this->placeholders = array_keys($this->placeholders_props);
		if (! is_array($props)):
			global $sys;
			$sys->terminate('Formatting requires a properties array to be constructed', ERR_ITEM);
		endif; // passed props are not an array
		$this->properties = $props;
	} // __construct()
	private function formatProps($format) {
		// properties interpolation
		$out = '';
		$placeholder_pattern = '/(\\\\?)%./';
		while (1 === preg_match($placeholder_pattern,
			$format, $matches, PREG_OFFSET_CAPTURE)
		):
			$group = $matches[FMT_MATCH_FULL][FMT_MATCH_STRING];
			$offset = $matches[FMT_MATCH_FULL][FMT_MATCH_OFFSET];
			$group_size = strlen($group);
			// determine replacement:
			$property = $group[$group_size-1];
			if (FMT_ESC_START === $group[0]):
				// the match starts with a '\' (escape sequence)
				$replacement = substr($group, 1);
			elseif (in_array($property, $this->placeholders)):
				// valid placeholder
				$replacement = $this->properties[
					$this->placeholders_props[$property]];
			else:
				// property for the found placeholder does not exist
				$replacement = $group;
			endif; // group
			$format = substr_replace($format,
				$replacement, $offset, $group_size);
			$processed_size = $offset + strlen($replacement);
			$out = $out.substr($format, 0, $processed_size);
			$format = substr($format, $processed_size);
		endwhile; // placeholders
		$out = $out.$format;
		return $out;
	} // formatProps()
	private function formatEscs($in) {
		// substitute escape sequences
		$escapes = array
		(
			FMT_ESC_START => FMT_ESC_START,
			'n' => PHP_EOL,
			't' => "\t"
		); // escapes
		$out = '';
		$escseq_pattern = '/(?<='.str_repeat(FMT_ESC_START, 2).')./';
		while (1 === preg_match($escseq_pattern,
			$in, $matches, PREG_OFFSET_CAPTURE)
		):
			$offset = $matches[FMT_MATCH_FULL][FMT_MATCH_OFFSET];
			$chr = $matches[FMT_MATCH_FULL][FMT_MATCH_STRING];
			if (array_key_exists($chr, $escapes)):
				$current_pattern = '/'.str_repeat(FMT_ESC_START, 2).
					str_repeat($chr, FMT_ESC_START === $chr? 2: 1).'/';
				$in = preg_replace($current_pattern,
					$escapes[$chr], $in, 1);
			else:
				$offset = $offset + 1;
			endif; // replace
			// concatenate
			$out = $out.substr($in, 0, $offset);
			$in = substr($in, $offset);
		endwhile; // preg_match
		$out = $out.$in;
		return $out;
	} // formatEscs()
	function format($format) {
		$out = $this->formatEscs($format);
		$out = $this->formatProps($out);
		return $out;
	} // format()
} // FormattingXml

?>
