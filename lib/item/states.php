<?php namespace cornerstone\item;

class States {
	private $states;
	function __construct() {
		$this->states = array
		(
			ITEM_STATE_OPEN => new State(ITEM_STATE_OPEN, 'opened'),
			ITEM_STATE_DONE => new State(ITEM_STATE_DONE, 'done'),
			ITEM_STATE_TRASH => new State(ITEM_STATE_TRASH, 'removed')
		); // states array
	} // function __construct
	function __get($state) {
		if (! array_key_exists($state, $this->states)):
			global $sys;
			$sys->terminate("The '$state' does not exist in the instance of item\State", ERR_ITEM);
		endif; // ! state exists
		return $this->states[$state];
	} // function __get
} // class States

?>
