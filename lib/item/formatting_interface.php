<?php namespace cornerstone\item;

interface FormattingInterface {
	function format($format);
} // FormattingInterface

?>
