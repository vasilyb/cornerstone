<?php namespace cornerstone\item;
use cornerstone\Item as Item;
use cornerstone\xml\Deserializer;

class ReadingXml implements ReadingInterface
{
	private function fileReadXml($file_pointer) {
		$meta_data = stream_get_meta_data($file_pointer);
		$filename = $meta_data['uri'];
		if (! $filename):
			return array(); // 'coz unable to get file size
		endif; // couldn't obtain the URI from file pointer
		$xml = fread($file_pointer, filesize($filename));
		if (! $xml):
			return array(); // 'coz didn't get XML
		endif; // couldn't read the file
		$parser = new Deserializer;
		$root = $parser->xmlParseIntoArray($xml);
		$item = $root['item'];
		return $item? $item: array();
	} // fileReadXml()

	function fileRead($path) {
		// reads an item from a file
		$item = new Item;
		if (! is_dir($path)):
			$item->id = pathinfo($path, PATHINFO_FILENAME);
			$file = fopen($path, 'r');
			if (! $file):
				$item->title = "(Failed opening '$path' for read)";
			else: // fopen
				$array = $this->fileReadXml($file);
				if (! array_key_exists(ITEM_PROP_TITLE, $array)):
					$item->title = "(Failed to read '$path')";
				else: // $array[ITEM_PROP_TITLE] exists
					$item->title = $array[ITEM_PROP_TITLE];
				endif;
				if (!! $file && ! fclose($file)):
					$item->title = $item->title.
						" (Unable to properly close '$path')";
				endif; // ! fclose
			endif; // ! fopen
			$item->state = pathinfo(pathinfo($path, PATHINFO_DIRNAME),
				PATHINFO_BASENAME);
			return $item;
		else: // path is dir
			return false;
		endif;
	} // fileRead()
} // ReadingXml

?>
