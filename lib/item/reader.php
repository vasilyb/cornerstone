<?php namespace cornerstone\item;

class Reader implements ReadingInterface
{
	private $reading;
	function __construct() {
		$this->reading = new ReadingXml;
	} // __construct()
	function fileRead($path) {
		return $this->reading->fileRead($path);
	} // fileRead()
} // Reader

?>
