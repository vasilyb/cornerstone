<?php namespace cornerstone\item;

class Writer implements WritingInterface
{
	private $writing;
	function __construct($item) {
		if (! isset($item)):
			global $sys;
			$sys->terminate('The constructor of item writer requires an item.',
				ERR_ITEM);
		endif;
		$this->writing = new WritingXml($item);
	} // __construct()
	function fileWrite($dir) {
		return $this->writing->fileWrite($dir);
	} // fileWrite()
} // Reader

?>
