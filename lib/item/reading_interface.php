<?php namespace cornerstone\item;

interface ReadingInterface
{
	function fileRead($path);
} // ReadingInterface

?>
