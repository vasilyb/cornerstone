<?php namespace cornerstone\item;

class Formatter implements FormattingInterface
{
	private $formatting;
	function __construct($props = array()) {
		if (0 === count($props)):
			global $sys;
			$sys->terminate('No properties passed to formatter.', ERR_ITEM);
		endif; // no properties passed
		$this->formatting = new Formatting($props);
	} // __construct()
	function format($format) {
		return $this->formatting->format($format);
	} // format()
} // Formatter

?>
