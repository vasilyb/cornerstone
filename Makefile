# $FreeBSD$

PORTNAME=		cornerstone
PORTVERSION=	0.3
CATEGORIES=		deskutils misc
MASTER_SITES=	https://gitlab.com/vasilyb/cornerstone/wikis/downloads/

MAINTAINER=		Vasily.Blinkov@gmail.com
COMMENT=		Task management application
LICENSE=		MIT

# Own variables to simplify installation instructions.
STAGE_LIBS=		${STAGEDIR}${PREFIX}/lib/${PORTNAME}
COMPOSER=		/vendor/composer/
XMTK=			/vendor/xmtk/xmtk/src/

FETCH_DEPENDS=	composer:devel/php-composer

NO_BUILD=		yes

post-extract:
	# install dependencies using composer
	(cd ${INSTALL_WRKSRC} && composer install)
	# create dirs for libs, vendor scripts and composer
	${MKDIR} ${STAGE_LIBS}${COMPOSER}
	# copy composer itself
	(cd ${WRKSRC}${COMPOSER} && ${COPYTREE_SHARE} . ${STAGE_LIBS}${COMPOSER})
	# copy composer autoloader
	${INSTALL_DATA} ${INSTALL_WRKSRC}/vendor/autoload.php ${STAGE_LIBS}/vendor/
	# copy only required files from xmtk
	${MKDIR} ${STAGE_LIBS}${XMTK}
	(cd ${WRKSRC}${XMTK} && ${COPYTREE_SHARE} . ${STAGE_LIBS}${XMTK})

do-install:
	# main executable script
	${INSTALL_SCRIPT}\
	${INSTALL_WRKSRC}/${PORTNAME}\
	${STAGEDIR}${PREFIX}/bin/
	# manual
	${INSTALL_MAN}\
	${INSTALL_WRKSRC}/man/${PORTNAME}.1\
	${STAGEDIR}${MANPREFIX}/man/man1/
	# create namespaces
	for d in `cd ${INSTALL_WRKSRC}/lib/ && find . -type d`\
	;do\
		${MKDIR} ${STAGE_LIBS}/$$d\
	;done
	# copy libraries
	for l in `cd ${INSTALL_WRKSRC}/lib/ && find . -type f`\
	;do\
		${INSTALL_DATA}\
		${INSTALL_WRKSRC}/lib/$$l\
		${STAGE_LIBS}/$$l\
	;done
	# create the directory for the commands
	${MKDIR} ${STAGEDIR}${DATADIR}
	${MKDIR} ${STAGEDIR}${DATADIR}/commands
	(cd ${WRKSRC}/commands && ${COPYTREE_SHARE} . ${STAGEDIR}${DATADIR}/commands)

USES=			php:cli
USE_PHP=		xml
DEFAULT_VERSIONS=	php=70
IGNORE_WITH_PHP=	56

.include <bsd.port.mk>
