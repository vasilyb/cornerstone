<?php namespace cornerstone\cmnd;

use cornerstone\Fs;
use cornerstone\item;

class Lst extends base\Cmnd {
	function __construct($options, $main_arg = NULL) {
		parent::__construct($options, $main_arg);
		if (empty($this->options['format'])):
			$this->options['format'] = '%i %t';
		endif; // empty format
		if (empty($this->options[ITEM_PROP_STATE])):
			$this->options[ITEM_PROP_STATE] = ITEM_STATE_OPEN;
		endif; // empty states
	} // function __construct
	private function listItems($state) {
		$dir = $this->storage->of(ITEM_TYPE_TODO, $state);
		if (file_exists($dir)):
			$files = scandir($dir, SCANDIR_SORT_NONE);
		else:
			return;
		endif; // dir exists
		if (! $files):
			global $sys;
			$sys->terminate("Directory scan failed: '$dir'",
				ERR_CMND);
		endif; // ! files
		sort($files, SORT_NUMERIC);
		$i = 0;
		while ($i < sizeof($files)):
			$path = Fs::fileName($dir, $files[$i]);
			if (is_file($path)):
				$item = Item::fileRead($path);
				if ($item != false):
					echo $item->format($this->options['format']).PHP_EOL;
				endif; // item != false
			endif; // $path is a file
			$i = $i + 1;
		endwhile; // sizeof $files
	} // listItems()
	private function explodeStatus() {
		$states = explode(CLI_PARAMVAL_DELIMITER, $this->options[ITEM_PROP_STATE]);
		$correct = array();
		$wrong = array();
		// make arrays
		foreach ($states as $state):
			if (item\State::test($state)):
				$correct[] = $state;
			else: // wrong state
				$wrong[] = $state;
			endif;
		endforeach; // status
		// return result or die
		if (empty($wrong)):
			// sort by status
			$states = array();
			foreach (item\State::all() as $state):
				if (in_array($state, $correct)):
					$states[] = $state;
				endif; // state selected
			endforeach; // available states
			return $states;
		else: // wrong
			$wrong = implode(', ', $wrong);
			global $sys;
			$sys->terminate("The next values are not valid for --status option: $wrong.",
				ERR_CMND);
		endif;
	} // explodeStatus()
	function exec() {
		foreach ($this->explodeStatus() as $dir):
			$this->listItems($dir);
		endforeach; // status
	} // function exec
} // command Lst

?>
