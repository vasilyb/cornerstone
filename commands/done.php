<?php namespace cornerstone\cmnd;

class Done extends base\Move {
	function __construct($options = array(), $item_id) {
		parent::__construct($options, $item_id);
		$this->opts = new auxiliary\MoveCmndOpts($this->states->open,
			$this->states->done);
	} // function __construct
} // command Done

?>
