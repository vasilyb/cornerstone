<?php namespace cornerstone\cmnd;

class Restore extends base\Move {
	function __construct($options = array(), $item_id) {
		parent::__construct($options, $item_id);
		$this->opts = new auxiliary\MoveCmndOpts($this->states->trash,
			$this->states->open);
	} // function __construct
} // command Restore

?>
