<?php namespace cornerstone\cmnd;

class Remove extends base\Move {
	function __construct($options = array(), $item_id) {
		parent::__construct($options, $item_id);
		$this->opts = new auxiliary\MoveCmndOpts($this->states->open,
			$this->states->trash);
	} // function __construct
} // command Remove

?>
