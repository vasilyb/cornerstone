<?php namespace cornerstone\cmnd;

use cornerstone\System;

class Wrong extends base\Cmnd {
	private $cmnd_name = '';
	function __construct($options, $cmnd_name = NULL) {
		parent::__construct($options, $cmnd_name);
		$this->cmnd_name = isset($cmnd_name)?
			$cmnd_name:
			strtolower(str_replace(__NAMESPACE__.NS_DELIMITER, '', __CLASS__));
	} // function __construct
	function exec() {
		echo "The '{$this->cmnd_name}' is a wrong command.".
			PHP_EOL;

		global $sys;
		$cmnd_path = $sys->realPath($sys->cmnd(
			"{$this->cmnd_name}.php"));
		if (! file_exists($cmnd_path)):
			global $sys;
			$sys->terminate("It should be stored in the ".
					"'${cmnd_path}' file which is absent.",
				ERR_CMND);
		endif; // command file does not exist
	} // function exec
} // command Wrong

?>
