<?php namespace cornerstone\cmnd\base;

# this file contains the base class for commands
# specialized in changing states of items

use cornerstone\Fs;
use cornerstone\item;

abstract class Move extends Cmnd {
	private $id;
	protected $opts;
	protected $states;
	function __construct($options = array(), $item_id) {
		parent::__construct($options, $item_id);
		$this->id = $item_id;
		$this->opts = $options;
		$this->states = new item\States;
	} // function __construct
	function exec() {
		global $sys;
		if (! isset($this->id)):
			$sys->terminate(
				"Specify an ID of the item you want to mark as {$this->opts->target->name}.".PHP_EOL.
				'You can find IDs of items using the next command: `cornerstone list`.',
				ERR_CMND);
		endif;
		$src_name = Fs::fileName($this->storage->of(ITEM_TYPE_TODO, $this->opts->source->state),
			$this->id,
			EXT_ITEM);
		if (! file_exists($src_name)):
			$sys->terminate(
				"The item with ID = {$this->id} doesn't exist or not {$this->opts->source->name}.",
				ERR_CMND);
		endif;
		$target_dir = $this->storage->of(ITEM_TYPE_TODO, $this->opts->target->state);
		Fs::makePath($target_dir);
		$target_name = Fs::fileName($target_dir, $this->id, EXT_ITEM);
		rename($src_name, $target_name)
			or $sys->terminate(
				"Failed to mark the item with ID = {$this->id} as {$this->opts->target->name}",
				ERR_CMND);
	} // function exec
} // class Move

?>
