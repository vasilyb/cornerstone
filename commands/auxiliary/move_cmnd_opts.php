<?php namespace cornerstone\cmnd\auxiliary;

class MoveCmndOpts {
	public $source;
	public $target;
	function __construct($source, $target) {
		$this->source = $source;
		$this->target = $target;
	} // function __construct
} // class MoveCmndOpts

?>
