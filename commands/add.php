<?php namespace cornerstone\cmnd;

use cornerstone\Fs as Fs;
use cornerstone\Item as Item;

class Add extends base\Cmnd {
	private $title = '';
	function __construct($opts = array(), $title = '') {
		parent::__construct($opts, $title);
		$this->title = $title;
	} // function __construct
	function exec() {
		global $sys;
		if (empty($this->title)):
			$sys->terminate("The item must have a title", ERR_CMND);
		endif; // empty title
		$number = $this->storage->getHead() + 1;
		$item = new Item($this->title, $number);
		$dir = $this->storage->of(ITEM_TYPE_TODO, ITEM_STATE_OPEN);
		if (! $item->fileWrite($dir)):
			$sys->terminate("Failed to store the new item to disk.", ERR_CMND);
		endif; // $item failed to write a file
		$this->storage->setHead($number);
	} // function exec
} // command Add

?>
