<?php namespace cornerstone\cmnd;

class Show extends base\Cmnd {
	function __construct($options = array(), $argument) {
		parent::__construct($options, $argument);
	} // function __construct
	function exec() {
		echo 'showing the the individual to-do item'.PHP_EOL;
	} // function exec
} // command Show

?>
