<?php namespace cornerstone\cmnd;

class Reopen extends base\Move {
	function __construct($options = array(), $item_id) {
		parent::__construct($options, $item_id);
		$this->opts = new auxiliary\MoveCmndOpts($this->states->done,
			$this->states->open);
	} // function __construct
} // command Reopen

?>
