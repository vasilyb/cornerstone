# CORNERSTONE
# About
Cornerstone is a desktop application to store user appointments in the original XML format.

# History
I started the project in the October, 2016 when Lightning (which is Thunderbird add-in) caused my computer to down.
# Implementation details
## Dependencies
Cornerstone is implemented as a [PHP](http://php.net) [7.0](http://php.net/ChangeLog-7.php#7.0.24) script.
Its graphical user interface (GUI) will be implemented using [NCurses](http://php.net/manual/en/book.ncurses.php).

## User interface (UI)
Cornerstone has two user interfaces:
1. command line interface (CLI),
1. graphical user interface (in a future).

# Installation
Cornerstone is a quite young project in the beginning stage.
So it is not in the ports tree.
Maybe in the future I will send a proposal to review and commit Cornerstone to ports tree, but not today. :)

## Downloading the port
To install Cornerstone you should download the port using one of the next links.
[Download port](https://gitlab.com/vasilyb/cornerstone/wikis/downloads/cornerstone-0.1.port.tar.gz)

## Installation
Then unpack the port archive, go to the unpaked directory and install the port using `make`.

## How to install Cornerstone
Unified installation process is shown in the below example.
```bash
cd /tmp && mkdir -p cornerstone && cd cornerstone
wget https://gitlab.com/vasilyb/cornerstone/wikis/downloads/cornerstone-0.1.port.tar.gz
tar xvf cornerstone.port.tar.gz
sudo make install clean
```

# CONTRIBUTION

## First of all

1. Clone the repo.
1. Install composer locally or globally.
1. Run `php composer.phar install` if the Composer installed locally or `composer install` if it is installed globally to download dependencies.

## Composer

This project uses Composer.
To install it run the next commands when you are in the root project directory (level of the `cornerstone` file).
```bash
sudo pkg install php56-json php56-phar php56-filter php56-iconv php56-openssl php56-zlib
php composer-setup.php
```

Please, do not forget that the `composer.lock` file is intended to be commited!
Never commit `composer.phar` into the repository!
This is to prevent packages modifications by someone's hands without composer automation.
If you don't know what Composer is and why it is used, read the [Composer Introduction](https://getcomposer.org/doc/00-intro.md#introduction).

When running in a debug mode (from the source directory) `cornerstone` will die if the composer is not installed locally into the project.

By the way, Composer is required only for development needs, not for usage of Cornerstone.

## Releasing
This section describes how to automatically publish port and tarball of the Cornerstone.

There is a `release.mk` file in the source files. This is a Makefile with some simple targets:
* `port` -- to publish a new version of the FreeBSD port and
* `tarball` -- to create a `gzip`ped TAR of the Cornerstone.
* `clean` -- to clean up the work files,
* `all` -- to run the `tarball`, `port` and `clean` targets automatically.

I use the project's wiki as the master site for tarball and port.
The `release.mk` makefile prepares archives and pushes them to the wiki.
It's fully automated.
To use this file you must have to write access to the Cornerstone project's repositories.

### note 1
Your working directory must be a Git repository of Cornerstone sources.

### note 2
Using the next commands will cause the wiki downloading into `../cornerstone.wiki` directory.
The targers in the `release.mk` will not remove this directory.
Please feel free to use `../cornerstone.wiki` to edit the project's wiki.


## HOW TO RELEASE A NEW VERSION OF THE TARBALL AND THE PORT?
```bash
make -f release.mk all
```
This command will do all the work concerning creating the port and tarball.
Just to remember. The script probably will request the RSA key's password a couple times.
And, the `sudo` may request the password too. Look at the `release.mk` carefully.
