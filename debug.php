<?php namespace cornerstone;

if (! file_exists(__DIR__.'/composer.phar')):
	global $sys;
	$sys->terminate('Composer is not installed. '.
		'Please install it locally in order to continue contribution to the project. '.
		PHP_EOL.'Use the next two commands to setup Composer into the source.'.PHP_EOL.
		PHP_EOL.'1 $ sudo pkg install '.
		'php70-json php70-phar php70-filter php70-iconv php70-openssl php70-zlib'.
		PHP_EOL.'2 $ php composer-setup.php'.PHP_EOL.
		PHP_EOL.'Never commit the composer.phar file into the repository!',
		2/*ERR_DEBUG*/);
	/* ERR_DEBUG hard coded here because this file should be
	   included into the Cornerstone before defines.php */
endif; // composer is not installed

$sys->lib_dir = 'lib/';
$sys->data_dir = './';
$sys->vendor_dir = '../'.$sys->vendor_dir;

?>
