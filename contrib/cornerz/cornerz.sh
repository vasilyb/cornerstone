#!/usr/bin/env ksh93
# Show the to-do items from the Cornerstone

# Path to the todo-items application
corner='/mnt/maxtor/projects/cornerstone/cornerstone.git/cornerstone'

# Extended regular expression pattern for 2019-04-06 like dates stored in Cornerstone
ere_date_stor='([0-9]{4})-([0-1][0-9])-([0-3][0-9])'

# Window width
wwidth=860

# Extra button
add_item='Add a new item'
del_item='Remove current item'

function listItems {
    $corner list --format='%t|%i|'
}

function sortByDateAndTime {
    sort -k 1
}

function escapeSpace {
    sed -e 's/ /_/g'
}

function unescSpace {
    # Reverts escapeSpace()
    sed -e 's/_/ /g'
}

function escapeAmpersand {
    typeset text=$1
    typeset pango=`echo "$text" | sed -e 's/&/&amp;/g'`
    echo "$pango"
}

function splitTiming {
    # Transform '2019-04-06|20:00-22:00' to '2019-04-06|20:00|22:00'
    # to process start time and end time separately
    sed -E 's/(.{16})-/\1|/'
}

function getDateNumber {
    # Returns 20190406 from 2019-04-06 for dates comparison
    typeset item=$1
    echo "$item" | sed -E "s/$ere_date_stor.*\$/\1\2\3/"
}

function getState {
    # Parameters extraction
    typeset item=$1

    # Current date and time
    typeset cday=$(date +%Y%m%d)

    # Item date and time
    typeset iday=`getDateNumber $item`

    if [ "$iday" -lt "$cday" ]
    then echo 'Просрочено'
    elif [ "$iday" -gt "$cday" ]
    then echo 'Ожидается'
    else echo 'Сегодня'
    fi
}

function addState {
    while read -r item
    do
        echo "${item}`getState "$item"`|"
    done
}

function formatDate {
    # Transforms 2019-04-06 to 06.04.2019
    sed -E "s/^$ere_date_stor/\3.\2.\1/"
}

function selectItem {
    typeset list=''

    for item in `listItems \
        | sortByDateAndTime \
        | escapeSpace \
        | splitTiming \
        | addState \
        | formatDate`
    do
        list="$list$item"
    done

    list=`echo $list | unescSpace`

    # internal fields separator
    typeset ifs=$IFS
    IFS=$'|'

    zenity --title='Cornerstone'\
     --width=$wwidth\
     --height=480\
     --list\
     --text='Ваши дела'\
     --column='Дата'\
     --column='Начало'\
     --column='Окончание'\
     --column='Наименование'\
     --column='ID'\
     --column='Статус'\
     --hide-column=5\
     --print-column=5\
     --cancel-label='Выход'\
     --ok-label='Open the selected item'\
     --extra-button="$add_item"\
     $list

    IFS=$ifs
}

function completeItem {
	typeset iid=$1
	$corner done $iid
}

function deleteItem {
	typeset iid="$1"
  $corner remove "$iid"
}

function operateItem {
	typeset code="$1"
	typeset output="$2"
	typeset iid="$3"

	if [ -z "$output" ]
	then
		if [ "0" -eq "$code" ]
		then completeItem "$iid"
		fi

		return
	fi

	case "$output" in
	("$del_item") deleteItem "$iid" ;;
	esac
}

function viewItem {
	# obtain passed arguments
	typeset iid=$1

	# build information text
	typeset item=`$corner | grep "^$iid "`
	typeset title=`expr "$item" : "^$iid ....-..-..|..:..-..:..|\(.*\)"`
	typeset pangotitle=`escapeAmpersand "$title"`
	typeset itext="<span size='x-large'>$pangotitle</span>"

	output=`zenity\
	 --title="Cornerstone: item #$iid"\
	 --width=$wwidth\
	 --question\
	 --ok-label='Mark as done'\
	 --cancel-label='Go back to the list'\
	 --extra-button="$del_item"\
	 --text="$itext"`

	typeset complete=$?
	operateItem "$complete" "$output" "$iid"
}


function concatNumbers {
	# joins sequence of N Numbers with '|' glue

	typeset count="$1"
	let max=$count-1
	typeset nwidth=${#max}
	typeset out=''

	for n in `seq 0 "$max"`
	do
		add=''
		h=$n

		# separator
		if [ "${#out}" -ne 0 ]
		then add="|"
		fi

		# pad left
		while [ "${#h}" -lt "$nwidth" ]
		do h="0$h"
		done


		add="$add$h"
		out="$out$add"
	done

	echo "$out"
}

function extractFromStart {
	# gets substrings of an input string until a delimiter

	typeset str="$1"
	typeset delim="$2"
	typeset default="$3"

	if [ "${str#|}" != '|' ]
	then typeset val=`expr "$str" : "\([^$delim]*\)"`
	else val="${str/\|/}"
	fi
	echo "${val:-$default}"
}

function deleteFromStart {
	# removes the substring until (and including) the first delimiter

	typeset str="$1"
	typeset delim="$2"

	# a first argument with its delimiter
	typeset pattern="[^$delim]*$delim\(.*\)"
	if [ "${str#|}" != '|' ]
	then
		typeset str="`expr "$str" : "$pattern"`"
	else
		typeset str="${str/\|/}"
	fi

	# trim left
	str="${str# }"

	echo $str
}

function addItem {
	# requests item parameters from a user

	typeset hrs=`concatNumbers 24`
	typeset mins=`concatNumbers 60`
	typeset separator='|'

	typeset nitem=`zenity\
	 --forms\
	 --width=$wwidth\
	 --title='Cornerstone'\
	 --text='New appointment'\
	 --add-calendar='On date'\
	 --add-combo='From hour'\
	 --combo-values="$hrs"\
	 --add-combo='From minute'\
	 --combo-values="$mins"\
	 --add-combo='To hour'\
	 --combo-values="$hrs"\
	 --add-combo='To minute'\
	 --combo-values="$mins"\
	 --add-entry='Tag'\
	 --add-entry='What are you planning?'\
	 --separator="$separator"\
	 --forms-date-format='%Y-%m-%d'` || exit_code=$?

	if [ '0' -ne "$exit_code" ]
	then return
	fi

	date=`extractFromStart "$nitem" "$separator"`
	nitem=`deleteFromStart "$nitem" "$separator"`
	fromhr=$(extractFromStart "$nitem" "$separator" "`date +%H`")
	nitem=`deleteFromStart "$nitem" "$separator"`
	frommin=$(extractFromStart "$nitem" "$separator" "`date +%M`")
	nitem=`deleteFromStart "$nitem" "$separator"`
	tohr=$(extractFromStart "$nitem" "$separator" "$((fromhr+1))")
	nitem=`deleteFromStart "$nitem" "$separator"`
	tomin=$(extractFromStart "$nitem" "$separator" "$frommin")
	nitem=`deleteFromStart "$nitem" "$separator"`
	tag=$(extractFromStart "$nitem" "$separator" "Default")
	nitem=`deleteFromStart "$nitem" "$separator"`
	title=$(extractFromStart "$nitem" "$separator" "(No title provided)")

	$corner add "$date|$fromhr:$frommin-$tohr:$tomin|${tag}. $title"
}


stop=false
until $stop
do
	iid=`selectItem`

	if [ "$add_item" == "$iid" ]
	then addItem ; continue
	fi

	if [ -n "$iid" ]
	then viewItem $iid
	else stop=true
	fi
done

