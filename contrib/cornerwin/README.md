CORNERWIN
=========

INTRO
-----

CornerWin is a wrapper written in PowerShell to run Cornerstone on Windows.
Use this README file and it will guide you through the setup process.

SETUP
-----

1. Install PHP.
1. If you installed PHP 8.2 or newer, then find and open the `php.ini` file, then find the `;extension=zip` line and uncomment it to make it look like `extension=zip`.
1. Install Composer.
1. Download Cornerstone sources to `%appdata%\..\Local\Cornerstone`.
1. Install `composer.phar` locally to Cornerstone directory above.
1. In the `%appdata%\..\Local\Cornerstone` directory execute: `composer i`.
1. Create `%APPDATA%\Home` directory.
1. Create `%APPDATA%\Home\.config` directory.
1. Create a new directory for the wrapper scripts: `%APPDATA%\Cornerstone`.
1. Place the `Cornerstone.ps1` script in the directory you just created.
1. You may need to open the PowerShell script's file properties and check the Unlock box to allow PowerShell to run this remote file.
1. Execute the next command with the escalated privileges: `Set-ExecutionPolicy Bypass` for newly installed Windows.
1. If Git is not installed, install it.
1. Also place the `Cornerstone.bat` file there.
1. Create a shortcut on the desktop to the `Cornerstone.bat`.
1. Optionally change the icon of the shortcut.
1. Install Google Drive for Windows.
1. Open the `Cornerstone.ps1` file and change the variables (described below) under the `# Settings` region.

### Variables

* `$global:sync_remote_dir` should point to the directory on your Google Disk drive where you push to and where you will pull snapshots of the Cornerstone database from. This is the directory to perform synchronization of the Cornerstone data between your computers. Example: `G:\Мой диск\system\cornerstone`. In that directory you should see ZIP archives like `cornerstone.zip`, `cornerstone-1.zip`, `cornerstone-2.zip`, etc. You should set the correct value of this variable to make the sync features (the `cornerpull` and the `cornerpush` commands) working
* `$global:sync_local_dir` is the Cornerstone configuration directory where the unpacked data is stored. Example: `"$env:APPDATA\Home\.config\cornerstone"`. Contents of the directory: a `todo` subdirectory, an `.inc` file. If you look them, then everything is going correctly. If you followed the manual above, you won't touch this variable at all
* `$global:sync_local_bkup_dir` used to store backups of the Cornerstone data before overwriting it by remote data when running `cornerpull`. Example: `"$env:USERPROFILE\Documents\CornerstoneBakups"`. You may not be interested in changing the value of this variable if you are satisfied by its default value

SCREENSHOTS
-----------

![Screenshot](https://gitlab.com/vasilyb/cornerstone/raw/master/contrib/cornerwin/Screenshot.png)

USAGE
-----

### Basic usage

Note, that the `cornerlist` function of the `Cornerstone.ps1` script sorts the to-do items by the date.
Therefore each item you create with `cornerstone add` **must** match the next regular expression:
`/^\[\d{4}-\d{2}-d{2}\] .+/`.
E.g.: `[2022-04-28] something`, where `2022-04-28` is the due date.

If this doesn't fit your requirements just don't use the `cornerelist` function, rewrite it, or write your own.

### Sync your data

It's possible to use the `cornerpush` and `cornerpull` functions for syncing your data across devices.
I use Google Drive on Windows, therefore I have the disk (G:) where all my cloud files are available.
Your cloud may do it another way (WebDAV for example).
You just need to set the location of your folder syncing with your cloud in `Cornerstone.ps1`.

![Screenshot](https://gitlab.com/vasilyb/cornerstone/raw/master/contrib/cornerwin/Flowchats.png)
