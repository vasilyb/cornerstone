# CORNERSTONE WRAPPER FOR WINDOWS.
# RUN THIS SCRIPT AND FIND FURTHER INSTRUCTIONS IN THE CONSOLE.

# Settings:
$global:sync_remote_dir = "G:\Мой диск\system\cornerstone"
$global:sync_local_dir = "$env:APPDATA\Home\.config\cornerstone"
$global:sync_local_bkup_dir = "$env:USERPROFILE\Documents\CornerstoneBakups"
# todo sync_remote_depth - max count of backups on remote
# todo sync_local_depth - max count of local backups

function global:prompt {"
> "}

Clear-Host

$env:HOME = "$env:USERPROFILE\AppData\Roaming\Home"

Write-Host "
    Cornerstone environment is ready.
    Functions:
    - cornerstone     : interact with Cornerstone,
    - cornerlist      : items ordered by due date,
    - corneredit <id> : edit a to-do item,
    - cornerpull      : pull from remote,
    - cornerpush      : push to remote.
"

$global:appdir = "$env:USERPROFILE\AppData\Local\Cornerstone"
$global:datadir = "$global:sync_local_dir"
$global:opendir = "$global:datadir\todo\open"

function global:cornerstone {
    php "$appdir\cornerstone" $args
}

function global:cornerlist {
    cornerstone list --format='%i\t%t' | `
        Sort-Object {($_ | `
            Select-String -Pattern '\d{4}-\d{2}-\d{2}' `
        ).Matches[0].Value}
}

function global:corneredit($id) {notepad "$opendir\$id.xml"}

function global:pack_local {
    $archivepath = [System.IO.Path]::GetTempFileName()

    $archivepath = [System.IO.Path]::Combine( `
        [System.IO.Path]::GetDirectoryName($archivepath), `
        [string]::Join("", [System.IO.Path]::GetFileNameWithoutExtension($archivepath), ".zip") `
    )

    #Compress-Archive -Path "$global:datadir" -DestinationPath "$archivepath"
    $location = (Get-Location).Path
    Set-Location ([System.IO.Path]::GetDirectoryName($Global:datadir))
    $dir = [System.IO.Path]::GetFileName($Global:datadir)
    tar -acf "$archivepath" "$dir"
    Set-Location $location

    $archivepath
}

function global:enumerate_archive($dir) {
    $actual = "$dir\cornerstone.zip"

    If ((Test-Path $actual -PathType Leaf)) {
        $items = Get-ChildItem "$dir" -Filter "cornerstone-*.zip"
        $next = 1

        If (($items | Measure-Object).Count -gt 0) {
            $next = [long]::Parse( `
                (($items | Select-Object -Property @{label="id";expression={[regex]::Match($_.Name, "(?<=cornerstone-)\d+(?=\.zip)").Value}}) `
                | Sort-Object -Property {[long]::Parse($_.id)} `
                | Select-Object -Last 1).id `
            ) + 1
        }

        Rename-Item -Path "$actual" -NewName "cornerstone-$next.zip"
    }
}

function global:pack_to($dir) {
    enumerate_archive "$dir"
    $archivepath = pack_local

    If (!(Test-Path "$dir" -PathType Container)) {
        $parent = [System.IO.Path]::GetDirectoryName("$dir")
        $name = [System.IO.Path]::GetFileName("$dir")

        New-Item -Path $parent -Name $name -ItemType Directory `
            | Out-Null
    }

    Move-Item -Path "$archivepath" -Destination "$dir\cornerstone.zip"
}

function global:cornerpull() {
    $remotefile = global:get_remote_file_name
    If (!$remotefile) {
        Write-Host "No remote data found at '$global:sync_remote_dir\'"
        Return
    }
    
    If (Test-Path "$global:datadir" -PathType Container) {
        pack_to "$Global:sync_local_bkup_dir"
        Remove-Item -Path "$Global:datadir" -Recurse
    } Else {
        Write-Host "No local data found at '$global:datadir\'"
    }

    $destdir = [System.IO.Path]::GetDirectoryName($global:datadir)
    $destfile = "$destdir\cornerstone.zip"
    
    Copy-Item -Path "$Global:sync_remote_dir\$remotefile" -Destination "$destdir\cornerstone.zip"

    $lastdir = (Get-Location).Path
    Set-Location "$destdir"
    tar -xf "$destfile"
    Set-Location "$lastdir"

    Remove-Item -Path "$destfile"
}

function global:get_remote_file_name() {
    If (!(Test-Path "$Global:sync_remote_dir" -PathType Container)) {
        Return $false
    }

    If ((Get-ChildItem "$Global:sync_remote_dir" `
        | Where-Object -Property Name -EQ "cornerstone.zip" `
        | Measure-Object).Count -eq 1
    ) {
        Return "cornerstone.zip"
    }

    $numbered_archives = Get-ChildItem "$Global:sync_remote_dir" `
        | Where-Object -Property Name -Match "cornerstone-\d+\.zip"

    If (($numbered_archives | Measure-Object).Count -gt 0) {
        Return ($numbered_archives `
            | Sort-Object -Descending {[int](($_.Name `
            | Select-String "(?<=cornerstone-)\d+(?=\.zip)").Matches[0].Value)})[0].Name
    }

    Return $false
}

function global:cornerpush() {
    pack_to "$Global:sync_remote_dir"
}

cornerlist

# TODO: Prevent crashes in the cornerlist if an item does
#       not have a [proper] due date.
# TODO: Remove the user name from locations (use %APPDATA%).
